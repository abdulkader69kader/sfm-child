<?php
$author_id            = get_current_user_id();
$user_profile_post_id = get_user_meta( $author_id, 'user_profile_id', true );
?>
<form action="" method="POST" id="employer-profile-edit-form" class="edit-form validation-enabled"
      enctype="multipart/form-data">
    <h3 class="profile-title">Personal Details</h3>
    <div class="personal-details">
        <div class="three-column-row">
            <div class="input-field">
                <label for="first_name">First Name</label>
                <input id="first_name" type="text" name="first_name" placeholder="First Name"
                       value="<?php echo get_the_author_meta( 'first_name', $author_id ); ?>" required>
            </div>
            <div class="input-field">
                <label for="last_name">Last Name</label>
                <input id="last_name" type="text" name="last_name" placeholder="Last Name"
                       value="<?php echo get_the_author_meta( 'last_name', $author_id ); ?>" required>
            </div>
            <div class="input-field">
                <label for="display_name">Display Name</label>
                <input id="display_name" type="text" name="display_name" placeholder="Public Name"
                       value="<?php echo get_the_author_meta( 'display_name', $author_id ); ?>" required>
            </div>
            <div class="input-field">
                <label for="phone_number">Phone Number</label>
                <input id="phone_number" type="text" name="phone_number" placeholder="Your Phone Number"
                       value="<?php echo get_the_author_meta( 'phone_number', $author_id ); ?>" required>
            </div>
            <div class="input-field">
                <label for="company_name">Company Name</label>
                <input id="company_name" type="text" name="company_name" placeholder="Company Name"
                       value="<?php echo get_the_author_meta( 'company_name', $author_id ); ?>" required>
            </div>
            <div class="input-field">
                <label for="job_title">Job Title</label>
                <input id="job_title" type="text" name="job_title" placeholder="Job Title"
                       value="<?php echo get_the_author_meta( 'job_title', $author_id ); ?>" required>
            </div>
        </div>

        <div class="input-field fre-input-field details">
            <label class="fre-field-title" for="project_category">Interested Project
                Categories</label>
		    <?php
		    $selected_cats = get_the_terms( $user_profile_post_id, 'project_category' );
		    $cat_arr       = [];
		    if ( ! empty( $selected_cats ) ) {
			    foreach ( $selected_cats as $cat ) {
				    $cat_arr[] = $cat->term_id;
			    }
		    }

		    ae_tax_dropdown( 'project_category',
			    array(
				    'attr'            => 'data-chosen-width="100%" required data-chosen-disable-search="" multiple data-placeholder="' . sprintf( __( "Choose categories (max %s) ", ET_DOMAIN ), ae_get_option( 'max_cat', 5 ) ) . '"',
				    'class'           => 'fre-chosen-multi required',
				    'hide_empty'      => false,
				    'hierarchical'    => true,
				    'id'              => 'project_category',
				    'show_option_all' => false,
				    'selected'        => $cat_arr,
				    'name'            => 'project_category[]'
			    )
		    )
		    ?>
        </div>

        <div class="input-field details">
            <label for="describe_more">Describe more details about you</label>
            <textarea id="describe_more" name="describe_more" rows="20"
                      cols="20"><?php echo get_the_author_meta( 'describe_more', $author_id ); ?></textarea>
        </div>
    </div>
    <h3 class="profile-title">Social Profiles</h3>
    <div class="social-details">
        <div class="input-field">
            <label for="facebook">Facebook</label>
            <input id="facebook" type="text" name="facebook" placeholder="https://facebook.com/yourusername"
                   value="<?php echo get_the_author_meta( 'facebook', $author_id ); ?>">
        </div>
        <div class="input-field">
            <label for="twitter">Twitter</label>
            <input id="twitter" type="text" name="twitter" placeholder="https://twitter.com/yourusername"
                   value="<?php echo get_the_author_meta( 'twitter', $author_id ); ?>">
        </div>
        <div class="input-field">
            <label for="linkedin">LinkedIn</label>
            <input id="linkedin" type="text" name="linkedin" placeholder="https://linkedin.com/yourusername"
                   value="<?php echo get_the_author_meta( 'linkedin', $author_id ); ?>">
        </div>
        <div class="input-field">
            <label for="skype">Skype</label>
            <input id="skype" type="text" name="skype" placeholder="your_skype_name"
                   value="<?php echo get_the_author_meta( 'skype', $author_id ); ?>">
        </div>
    </div>
    <h3 class="profile-title">Your Location</h3>
    <div class="location-details">
        <div class="input-field">
            <label for="country_you_live">Country You Live</label>
            <div class="select-box">
                <div class="select_icon"
                     style="background-image: url('http://sfm.idevs.site/wp-content/themes/freelanceengine-child/inc/images/select-icon.svg');">
                </div>
                <select name="country_you_live" id="country_you_live" class="sfm-select2" required>
                    <option value="">Select Country</option>
					<?php
					$countries           = get_terms( array(
						'taxonomy'   => 'country',
						'hide_empty' => false,
					) );
					$selected_country_id = get_the_terms( $user_profile_post_id, 'country' );
					foreach ( $countries as $country ) {
						if ( $selected_country_id ) {
							echo '<option value="' . $country->term_id . '" ' . ( $selected_country_id[0]->term_id == $country->term_id ? 'selected' : '' ) . '>' . $country->name . '</option>';
						} else {
							echo '<option value="' . $country->term_id . '">' . $country->name . '</option>';
						}
					}
					?>
                </select>
            </div>
        </div>
        <div class="input-field">
            <label for="city_name">City Name</label>
            <input id="city_name" type="text" name="city_name" placeholder="City name"
                   value="<?php echo get_the_author_meta( 'city_name', $author_id ); ?>" required>
        </div>
    </div>
    <h3 class="profile-title">Profile Picture</h3>
    <div class="picture-change">
        <div class="profile-image">
            <img src="<?php echo( get_user_meta( $author_id, 'et_avatar_url', true ) ? get_user_meta( $author_id, 'et_avatar_url', true ) : get_avatar_url( $author_id ) ); ?>"
                 alt="Profile Image">
        </div>
        <div class="upload-file">
            <label for="input-file-now">Change your profile picture</label>
            <div class="file-upload-wrapper">
                <input type="file" onchange="readURL(this);" name="profile_image" id="input-file-now"
                       class="file-upload"/>
                <label class="custom-file-label" for="input-file-now">Upload Picture</label>
            </div>
            <p>Accepted image format: 'png', 'jpg', 'jpeg', 'gif'</p>
        </div>
    </div>

	<?php wp_nonce_field( 'edit_profile', 'edit_profile_nonce' ); ?>

    <button class="btn-all ie_btn" type="submit" name="submit">Save all information</button>
</form>